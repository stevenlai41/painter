/**
 * @(#)hw101.java
 *
 *
 * @author
 * @version 1.00 2015/7/3
 */

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class hw10 extends JFrame implements ActionListener, MouseMotionListener
{
	private Container c;
	private JButton bt1,bt2,bt3,bt4,bt5,bt6,bt7,bt8,bt9;
	int px1,py1,px2,py2;
	private JLabel lb1;
	private JPopupMenu pm;
	private JMenuItem mi1,mi2,mi3;
	private int cr,cb,cg, xWidth = 20,yWidth = 20;

	public hw10()
	{
		super("Painter");
		c = getContentPane();
		c.setBackground(Color.white);
		addMouseMotionListener(this);

		JToolBar jtb = new JToolBar();
		bt1 = new JButton("Yellow");
		bt1.addActionListener(this);

		bt2 = new JButton("Green");
		bt2.addActionListener(this);

		bt3 = new JButton("Red");
		bt3.addActionListener(this);

		bt4 = new JButton("Blue");
		bt4.addActionListener(this);

		bt5 = new JButton("Black");
		bt5.addActionListener(this);

		bt6 = new JButton("White");
		bt6.addActionListener(this);

		bt7 = new JButton("Exit");
		bt7.addActionListener(this);

		bt8 = new JButton("Thicker");
		bt8.addActionListener(this);

		bt9 = new JButton("Thinner");
		bt9.addActionListener(this);

		lb1 = new JLabel("Font Weight");
		lb1.setText("Font Weight:"+ xWidth);

		pm = new JPopupMenu();
		pm.add("Options");
		pm.addSeparator();
		pm.add(mi1 = new JMenuItem("Black Background"));
		mi1.addActionListener(this);
		pm.add(mi2 = new JMenuItem("White Background"));
		mi2.addActionListener(this);
		pm.add(mi3 = new JMenuItem("Repaint"));
		mi3.addActionListener(this);

		jtb.add(bt1);
		jtb.add(bt2);
		jtb.add(bt3);
		jtb.add(bt4);
		jtb.add(bt5);
		jtb.add(bt6);
		jtb.add(bt7);
		jtb.add(bt8);
		jtb.add(bt9);
		jtb.add(lb1);

		c.add(jtb,BorderLayout.NORTH);

		addMouseListener(new MouseAdapter()
		{
			public void mouseClicked(MouseEvent e)
			{
				if (e.isMetaDown())
				{
					pm.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});
	}

	public static void main(String[] args)
	{
		hw10 win = new hw10();
		win.setSize(600, 600);
		win.setVisible(true);
		win.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}



	/*public void mouseMoved(MouseEvent e)
	{
		px1 = e.getX();
		py1 = e.getY();
	}

	public void mouseDragged(MouseEvent e)
	{
		px2 = e.getX();
		py2 = e.getY();

		Graphics g = getGraphics();

		g.drawLine(px1,py1,px2,py2);

		px1 = px2;
		py1 = py2;

	}*/

	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource() == bt1)
		{
			cr = 255;
			cg = 255;
			cb = 0;
		}
		else if(e.getSource() == bt2)
		{
			cr = 0;
			cg = 255;
			cb = 0;
		}
		else if(e.getSource() == bt3)
		{
			cr = 255;
			cg = 0;
			cb = 0;
		}
		else if(e.getSource() == bt4)
		{
			cr = 0;
			cg= 0;
			cb = 255;
		}
		else if(e.getSource() == bt5)
		{
			cr = 0;
			cg = 0;
			cb = 0;
		}
		else if(e.getSource() == bt6)
		{
			cr = 255;
			cg = 255;
			cb = 255;
		}
		else if(e.getSource() == bt7)
		{
			System.exit(0);
		}

		if(e.getSource() == mi1)
		{
			c.setBackground(Color.black);
		}
		else if(e.getSource() == mi2)
		{
			c.setBackground(Color.white);
		}
		else if(e.getSource() == mi3)
		{
			repaint();
		}

		if(e.getSource() == bt8)
		{
			xWidth += 5;
			yWidth += 5;
			lb1.setText("Font Weight:"+ xWidth);
		}
		else if(e.getSource() == bt9)
		{
			yWidth -= 5;
			xWidth -= 5;
			lb1.setText("Font Weight:"+ xWidth);
		}
	}

	public void mouseMoved(MouseEvent e)
	{
		px1 = e.getX();
		py1 = e.getY();
	}

	public void mouseDragged(MouseEvent e)
	{

		Graphics g = getGraphics();

		px2 = e.getX();
		py2 = e.getY();

		g.setColor(new Color(cr, cg, cb));

		g.fillOval(px1,py1, xWidth, yWidth);

		px1 = px2;
		py1 = py2;
	}
}